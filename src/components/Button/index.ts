import Button, { Props, Color } from './Button'

export default Button
export { Color }
export type { Props }
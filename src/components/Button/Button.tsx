import { ButtonHTMLAttributes } from 'react'

export enum Color {
  PRIMARY,
  RED
}

const COLOR_CLASSNAME_MAP: Record<Color, string> = {
  [Color.PRIMARY]: 'bg-blue-500',
  [Color.RED]: 'bg-red-500'
}

export type Props = Omit<ButtonHTMLAttributes<HTMLButtonElement>, 'color'> & {
  color?: Color
}

const Button = (props: Props): JSX.Element => {
  const {
    className,
    color = Color.PRIMARY,
    ...buttonAttrs
  } = props

  const classNameList = [
    'rounded',
    'border-none',
    'p-2',
    'text-base',
    'text-white',
    'disabled:opacity-50',
    COLOR_CLASSNAME_MAP[color],
    className
  ]

  return (
    <button
      className={classNameList.join(' ').trim()}
      {...buttonAttrs}
    />
  )
}
export default Button
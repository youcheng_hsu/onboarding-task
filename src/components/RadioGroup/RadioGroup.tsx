export type RadioItem = {
  id: string
  name: string
}

export type Props = {
  name: string

  value?: string

  onChange?(val: string): void

  radioList: RadioItem[]
}

const RadioGroup = (props: Props): JSX.Element => {
  const { name, value, onChange, radioList } = props
  return (
    <>
      {radioList.map((radio) => (
        <div
          key={radio.id}
          className="inline"
          onChange={() => onChange && onChange(radio.id)}
        >
          <input
            type="radio"
            id={radio.id}
            name={name}
            value={radio.id}
            checked={value === radio.id}
            readOnly
          />
          <label htmlFor={radio.id}>
            {radio.name}
          </label>
        </div>
      ))}
    </>
  )
}
export default RadioGroup
import { ChangeEvent, InputHTMLAttributes, useCallback } from 'react'

export type Props = Omit<InputHTMLAttributes<HTMLInputElement>, 'onChange'> & {
  value?: string
  onChange?(val: string): void
}

const Input = (props: Props): JSX.Element => {
  const { className, value, onChange, ...inputAttrs } = props
  const onInputChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    const newValue = e.target.value
    if (onChange) {
      onChange(newValue)
    }
  }, [onChange])

  return (
    <input
      {...inputAttrs}
      className={`p-2 border border-solid border-gray-300 text-base rounded ${className || ''}`}
      value={value}
      onChange={onInputChange}
    />
  )
}
export default Input
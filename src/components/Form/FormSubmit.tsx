import { useContext } from 'react'
import Button, { Props as ButtonProps } from '../Button'
import Context from './FormContext'

export type Props = Omit<ButtonProps, 'type'>

const FormSubmit = (props: Props): JSX.Element => {
  const { isSubmittable } = useContext(Context)
  return (
    <Button
      {...props}
      type="submit"
      disabled={!isSubmittable}
    />
  )
}
export default FormSubmit
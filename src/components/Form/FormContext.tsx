import { createContext, useCallback, useMemo, useRef, useState } from 'react'

export type ContextValue<F = Record<string, string>> = {
  form: F
  formError: Record<keyof F, string | undefined>
  isSubmittable: boolean
  setFormField(name: string, val: string): void
  setForm(form: F): void
}

const Context = createContext<ContextValue>({
  form: {},
  formError: {},
  isSubmittable: false,
  setFormField() {},
  setForm() {}
})
export default Context

export type Validator = {
  /**
   * Return error message is validate fail. empty string is considered validated pass.
   */
  (val: string): string | void
}

export function useContextValue(
  isDisabled: boolean,
  validate?: Record<string, Validator>
): ContextValue {
  const validateRef = useRef(validate)
  validateRef.current = validate

  const [form, setForm] = useState<Record<string, string>>({})
  const [formError, setFormError] = useState(() => {
    const result: Record<string, string | undefined> = {}
    const validate = validateRef.current
    if (validate) {
      Object.entries(validate).forEach(([key, isValid]) => {
        result[key] = isValid(form[key]) || undefined
      })
    }

    return result
  })
  
  const setFormField = useCallback((name: string, val: string) => {
    setForm((prev) => ({
      ...prev,
      [name]: val
    }))
    const isValid = validateRef.current && validateRef.current[name]
    setFormError((prev) => ({
      ...prev,
      [name]: isValid && (isValid(val) || undefined)
    }))
  }, [])

  return useMemo<ContextValue>(() => {
    let isSubmittable = !isDisabled && !Object.values(formError).some((val) => !!val)

    return {
      form,
      formError,
      isSubmittable,
      setFormField,
      setForm
    }
  }, [isDisabled, form, formError, setFormField])
}
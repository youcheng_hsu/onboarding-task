import { useCallback, useContext } from 'react'
import Context from './FormContext'
import style from './FormField.module.css'

export type InputProps = {
  name?: string
  value?: string
  onChange?(val: string): void
}

export type Props<P> = {
  className?: string
  name: string
  label: string
  inputTag(props: P & InputProps): JSX.Element
  inputProps: P & InputProps
}

function FormField<P>(props: Props<P>): JSX.Element {
  const {
    className,
    name,
    label,
    inputTag: InputTag,
    inputProps
  } = props

  const { form, formError, setFormField } = useContext(Context)
  const onInputChange = useCallback((value) => {
    setFormField(name, value)
  }, [name, setFormField])

  const value = form[name]
  const error = formError[name]

  return (
    <div className={`flex flex-col text-start ${className || ''}`}>
      <label>
        {label}
        {error && <small className='ml-1 text-red-500'>{error}</small>}
      </label>
      <InputTag
        {...inputProps}
        name={name}
        value={value || ''}
        onChange={onInputChange}
      />
    </div>
  )
}
export default FormField
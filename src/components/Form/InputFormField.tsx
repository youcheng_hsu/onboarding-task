import Input, { Props as InputProps } from '../Input'
import FormField, { Props as FormFieldProps } from './FormField'

export type Props = Omit<FormFieldProps<InputProps>, 'inputTag'>

const InputFormField = (props: Props): JSX.Element => {
  const {
    ...inputAttrs
  } = props

  return (
    <FormField
      {...inputAttrs}
      inputTag={Input}
    />
  )
}
export default InputFormField
import { FormEvent, PropsWithChildren, useState } from 'react'
import Context, { useContextValue, Validator } from './FormContext'

export type Props = PropsWithChildren<{
  className?: string
  validate?: Record<string, Validator>
  onSubmit(form: Record<string, string>): Promise<any> | void
}>

const Form = (props: Props): JSX.Element => {
  const { children, className, validate, onSubmit } = props
  const [isDisabled, setDisabled] = useState(false)
  const contextValue = useContextValue(isDisabled, validate)
  const onFormSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const submitResult = onSubmit(contextValue.form)
    if (submitResult) {
      setDisabled(true)
      submitResult
        .finally(() => {
          setDisabled(false)
        })
        .then(() => {
          contextValue.setForm({})
        })
    } else {
      contextValue.setForm({})
    }
  }

  return (
    <Context.Provider value={contextValue}>
      <form
        className={className}
        onSubmit={onFormSubmit}
      >
        {children}
      </form>
    </Context.Provider>
  )
}
export default Form
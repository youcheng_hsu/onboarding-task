import { PropsWithChildren } from 'react'

export type Props = PropsWithChildren<{
  className?: string
}>

const Container = (props: Props): JSX.Element => {
  const { className, children } = props
  return (
    <div className={`container mx-auto ${className || ''}`}>{children}</div>
  )
}
export default Container
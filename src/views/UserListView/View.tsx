import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Button, { Color } from '../../components/Button'
import { FetchState, fetchUserList, deleteUser, UserState } from '../../store'
import { User } from '../../types/User'

const TH_CLASSNAME = "border-b border-solid border-gray-200 text-start p-2"
const TD_CLASSNAME = "border-t border-solid border-gray-200 text-start p-2"

const UserListView = (): JSX.Element => {
  const users = useSelector<UserState, User[]>((state) => state.users)
  const dispatch = useDispatch()
  const [disableMap, setDisableMap] = useState<Record<number, boolean>>({})
  useEffect(() => {
    dispatch(fetchUserList())
  }, [dispatch])

  return (
    <table className="border-collapse w-full">
      <thead>
        <tr>
          <th className={TH_CLASSNAME}>Id</th>
          <th className={TH_CLASSNAME}>First Name</th>
          <th className={TH_CLASSNAME}>Last Name</th>
          <th className={TH_CLASSNAME}>Age</th>
          <th className={TH_CLASSNAME}>Gender</th>
          <th className={TH_CLASSNAME}>Address</th>
          <th className={TH_CLASSNAME} />
        </tr>
      </thead>
      <tbody>
        {users.map((user) => (
          <tr key={user.id}>
            <td className={TD_CLASSNAME}>{user.id}</td>
            <td className={TD_CLASSNAME}>{user.first_name}</td>
            <td className={TD_CLASSNAME}>{user.last_name}</td>
            <td className={TD_CLASSNAME}>{user.age}</td>
            <td className={TD_CLASSNAME}>{user.gender}</td>
            <td className={TD_CLASSNAME}>{user.address}</td>
            <td className={TD_CLASSNAME}>
              <Button
                color={Color.RED}
                disabled={disableMap[user.id]}
                onClick={() => {
                  setDisableMap((prev) => ({
                    ...prev,
                    [user.id]: true
                  }))
                  const fetchState = new FetchState()
                  dispatch(deleteUser(user, fetchState))
                  fetchState.promise
                    .finally(() => {
                      setDisableMap((prev) => {
                        const newMap = { ...prev }
                        delete newMap[user.id]

                        return newMap
                      })
                    })
                }}
              >
                Delete
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  )
}
export default UserListView
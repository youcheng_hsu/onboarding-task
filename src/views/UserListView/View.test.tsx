import React from 'react';
import { EventType, fireEvent, render, screen, waitFor } from '@testing-library/react';
import View from './View';
import { Provider } from 'react-redux';
import Store from '../../store';

jest.mock('../../service/User')

test('render user list', async () => {
  render(
    <Provider store={Store}>
      <View />
    </Provider>
  );
  
  await waitFor(() => expect(screen.getByText('Holmes')).toBeInTheDocument())

  const deleteButton = screen.getAllByText('Delete').pop()
  expect(deleteButton).toBeInTheDocument()
  await waitFor(() => fireEvent.click(deleteButton!))
  expect(deleteButton).not.toBeInTheDocument()
  expect(Store.getState().users.length).toBe(0)
});

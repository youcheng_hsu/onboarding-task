import React from 'react';
import { EventType, fireEvent, render, screen, waitFor } from '@testing-library/react';
import Form from './Form';
import { Provider } from 'react-redux';
import Store from '../../store';

jest.mock('../../service/User')

function mockEvent(content: string) {
  return {
    target: {
      value: content
    }
  }
}

test('render add-user form', async () => {
  render(
    <Provider store={Store}>
      <Form />
    </Provider>
  );
  
  const fieldMap = {
    first_name: [
      'change',
      screen.getByPlaceholderText('Enter First Name'),
      mockEvent('TEST_FIRST_NAME')
    ],
    last_name: [
      'change',
      screen.getByPlaceholderText('Enter Last Name'),
      mockEvent('TEST_LAST_NAME')
    ],
    age: [
      'change',
      screen.getByPlaceholderText('0'),
      mockEvent('100')
    ],
    male: [
      'click',
      screen.getByLabelText('Male')
    ],
    address: [
      'change',
      screen.getByPlaceholderText('Enter Address'),
      mockEvent('TEST_ADDRESS')
    ]
  }

  Object.values(fieldMap).forEach((tuple) => {
    const [name, target, option] = tuple as [EventType, HTMLElement, object]
    fireEvent[name as EventType](target, option)
  })

  await waitFor(() => fireEvent.click(screen.getByText('Add User')))

  const { users } = Store.getState()
  expect(users.length).toBe(1)
});

import React from 'react'
import { useDispatch } from 'react-redux'
import Form, { FormField, FormSubmit, InputFormField } from '../../components/Form'
import RadioGroup from '../../components/RadioGroup'
import { addUser, FetchState } from '../../store'
import { User } from '../../types/User'
import style from './Form.module.css'

const GenderRadioList = [
  { id: 'male', name: 'Male'},
  { id: 'female', name: 'Female'}
]

function validateExisted(value: string): string | void {
  if (!value) {
    return 'Required'
  }
}

const VALIDATOR_MAP = {
  first_name: validateExisted,
  last_name: validateExisted,
  age(val: string) {
    const isNotExisted = validateExisted(val)
    if (!isNotExisted) {
      return isNotExisted
    }

    let intVal = Number(val)
    if (isNaN(intVal)) {
      return 'Must be number'
    }

    if (intVal <= 0) {
      return 'Age must greater than 0'
    }
  },
  gender: validateExisted,
  address: validateExisted,
}

const AddUserForm = (): JSX.Element => {
  const dispatch = useDispatch()
  return (
    <Form
      className={style.main}
      validate={VALIDATOR_MAP}
      onSubmit={(form: Record<string, string>) => {
        const fetchState = new FetchState<User>()
        const gender = form.gender
        dispatch(addUser({
          first_name: form.first_name,
          last_name: form.last_name,
          gender: (gender === 'male' || gender === 'female') ? gender : 'male',
          age: parseInt(form.age),
          address: form.address
        }, fetchState))

        return fetchState.promise
      }}
    >
      <InputFormField
        name="first_name"
        label="First Name"
        inputProps={{
          placeholder: 'Enter First Name'
        }}
      />
      <InputFormField
        name="last_name"
        label="Last Name"
        inputProps={{
          placeholder: 'Enter Last Name'
        }}
      />
      <InputFormField
        name="age"
        label="Age"
        inputProps={{
          placeholder: '0'
        }}
      />
      <FormField
        name="gender"
        label="Gender"
        inputTag={RadioGroup}
        inputProps={{
          name: 'gender',
          radioList: GenderRadioList
        }}
      />
      <InputFormField
        name="address"
        label="Address"
        inputProps={{
          placeholder: 'Enter Address',
        }}
      />
      <FormSubmit>Add User</FormSubmit>
    </Form>
  )
}
export default AddUserForm
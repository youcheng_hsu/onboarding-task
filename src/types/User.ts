export type User = {
  id: number
  first_name: string
  last_name: string
  gender: 'male' | 'female'
  age: number
  address: string
}
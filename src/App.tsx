import { Provider } from 'react-redux'
import Store from './store'
import Container from './components/Container'
import UserListView from './views/UserListView'
import AddUserForm from './views/AddUserForm'
import './App.css'

function App(): JSX.Element {
  return (
    <Provider store={Store}>
      <Container className="App">
        <AddUserForm />
        <hr className="my-8 border-2 border-gray-200"/>
        <UserListView />
      </Container>
    </Provider>
  );
}

export default App;

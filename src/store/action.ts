import { User } from '../types/User'
import { ActionType, FetchAddUserAction, FetchDeleteUserAction, FetchUserListAction } from './reducer'
import FetchState from './FetchState'

export function fetchUserList(): FetchUserListAction {
  return {
    type: ActionType.FETCH_USER_LIST
  }
}

export function addUser(
  user: Omit<User, 'id'>,
  fetchState?: FetchState<User>
): FetchAddUserAction {
  return {
    type: ActionType.FETCH_ADD_USER,
    user,
    fetchState
  }
}

export function deleteUser(
  user: User,
  fetchState?: FetchState<void>
): FetchDeleteUserAction {
  return {
    type: ActionType.FETCH_DELETE_USER,
    user,
    fetchState
  }
}
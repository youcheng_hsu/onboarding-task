import Store from './store'
import { ActionType, UserState } from './reducer'
import FetchState from './FetchState'

export default Store
export { ActionType, FetchState }
export * from './action'
export type { UserState }
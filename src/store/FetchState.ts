export const RESOLVE_KEY = Symbol('resolve state')
export const REJECT_KEY = Symbol('reject state')

export default class FetchState<V = void> {
  promise: Promise<V>
  [RESOLVE_KEY]: (val: V) => void
  [REJECT_KEY]: (error?: Error) => void

  constructor() {
    this.promise = new Promise<V>((rs, rj) => {
      this[RESOLVE_KEY] = rs
      this[REJECT_KEY] = rj
    })
  }
}

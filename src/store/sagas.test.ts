import { waitFor } from '@testing-library/react'
import { call, put } from 'redux-saga/effects'
import { ActionType } from '.'
import * as UserService from '../service/User'
import { User } from '../types/User'
import FetchState from './FetchState'
import { addUser, deleteUser, fetchUserList } from './sagas'

jest.mock('../service/User.ts')

test('fetch user list', () => {
  const gen = fetchUserList()

  expect(gen.next().value).toEqual(call(UserService.getUserList))
  expect(gen.next().value).toEqual(put({
    type: ActionType.UPDATE_USER_LIST,
    users: undefined
  }))
})

const user: User = {
  "id": 1,
  "first_name": "Sherlock",
  "last_name": "Holmes",
  "gender": "male",
  "age": 39,
  "address": "221B Baker Street"
}

describe('add user', () => {
  it('success', () => {
    const gen = addUser({
      type: ActionType.FETCH_ADD_USER,
      user
    })

    expect(gen.next().value).toEqual(call(UserService.addNewUser, user))
    expect(gen.next().value).toEqual(put({
      type: ActionType.ADD_USER,
      user: undefined
    }))
  })

  it('success with fetchState', async () => {
    const fetchState = new FetchState<User>()
    const gen = addUser({
      type: ActionType.FETCH_ADD_USER,
      user,
      fetchState
    })

    expect(gen.next().value).toEqual(call(UserService.addNewUser, user))
    expect(gen.next().value).toEqual(put({
      type: ActionType.ADD_USER,
      user: undefined
    }))

    await fetchState.promise
      .then(() => expect(true).toBeTruthy())
      .catch(() => expect(true).toBeFalsy())
  })

  it('fail with fetchState', async () => {
    const fetchState = new FetchState<User>()
    const gen = addUser({
      type: ActionType.FETCH_ADD_USER,
      user,
      fetchState
    })

    expect(gen.next().value).toEqual(call(UserService.addNewUser, user))
    gen.throw('Error')
    await fetchState.promise
      .then(() => expect(false).toBeTruthy())
      .catch(() => expect(false).toBeFalsy())
  })
})

describe('delete user', () => {
  it('success', () => {
    const gen = deleteUser({
      type: ActionType.FETCH_DELETE_USER,
      user
    })

    expect(gen.next().value).toEqual(call(UserService.deleteUser, user))
    expect(gen.next().value).toEqual(put({
      type: ActionType.DELETE_USER,
      user
    }))
  })

  it('success with fetchState', async () => {
    const fetchState = new FetchState<void>()
    const gen = deleteUser({
      type: ActionType.FETCH_DELETE_USER,
      user,
      fetchState
    })

    expect(gen.next().value).toEqual(call(UserService.deleteUser, user))
    expect(gen.next().value).toEqual(put({
      type: ActionType.DELETE_USER,
      user
    }))
    await fetchState.promise
      .then(() => expect(true).toBeTruthy())
      .catch(() => expect(true).toBeFalsy())
  })

  it('fail with fetchState', async () => {
    const fetchState = new FetchState<void>()
    const gen = deleteUser({
      type: ActionType.FETCH_DELETE_USER,
      user,
      fetchState
    })

    expect(gen.next().value).toEqual(call(UserService.deleteUser, user))
    gen.throw('Error')
    await fetchState.promise
      .then(() => expect(false).toBeTruthy())
      .catch(() => expect(false).toBeFalsy())
  })
})
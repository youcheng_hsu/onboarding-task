import { put, call, takeEvery } from 'redux-saga/effects'
import * as UserService from '../service/User'
import { REJECT_KEY, RESOLVE_KEY } from './FetchState'
import { ActionType, FetchAddUserAction, FetchDeleteUserAction } from './reducer'

export function* fetchUserList() {
  const users = yield call(UserService.getUserList)
  yield put({
    type: ActionType.UPDATE_USER_LIST,
    users
  })
}

export function* addUser(action: FetchAddUserAction) {
  const { fetchState } = action
  try {
    const user = yield call(UserService.addNewUser, action.user)
    if (fetchState) {
      fetchState[RESOLVE_KEY](user)
    }
    yield put({
      type: ActionType.ADD_USER,
      user
    })
  } catch(err) {
    if (fetchState) {
      fetchState[REJECT_KEY](err)
    }
  }
}

export function* deleteUser(action: FetchDeleteUserAction) {
  const { user, fetchState } = action
  try {
    yield call(UserService.deleteUser, user)
    if (fetchState) {
      fetchState[RESOLVE_KEY]()
    }
    yield put({
      type: ActionType.DELETE_USER,
      user
    })
  } catch(err) {
    if (fetchState) {
      fetchState[REJECT_KEY](err)
    }
  }
}

export default function* userSaga() {
  yield takeEvery(ActionType.FETCH_USER_LIST, fetchUserList)
  yield takeEvery(ActionType.FETCH_ADD_USER, addUser)
  yield takeEvery(ActionType.FETCH_DELETE_USER, deleteUser)
}
import { User } from '../types/User'
import FetchState from './FetchState'

export enum ActionType {
  FETCH_USER_LIST = 'FETCH_USER_LIST',
  UPDATE_USER_LIST = 'UPDATE_USER_LIST',
  FETCH_ADD_USER = 'FETCH_ADD_USER',
  ADD_USER = 'ADD_USER',
  FETCH_DELETE_USER = 'FETCH_DELETE_USER',
  DELETE_USER = 'DELETE_USER'
}

type FetchStateAction<V> = {
  fetchState?: FetchState<V>
}

export type FetchUserListAction = {
  type: ActionType.FETCH_USER_LIST
}

type UpdateUserListAction = {
  type: ActionType.UPDATE_USER_LIST,
  users: User[]
}

export type FetchAddUserAction = FetchStateAction<User> & {
  type: ActionType.FETCH_ADD_USER,
  user: Omit<User, 'id'>
}

type AddUserAction = {
  type: ActionType.ADD_USER,
  user: User,
}

export type FetchDeleteUserAction = FetchStateAction<void> & {
  type: ActionType.FETCH_DELETE_USER,
  user: User
}

type DeleteUserAction = {
  type: ActionType.DELETE_USER,
  user: User
}

type Action =
  FetchUserListAction |
  UpdateUserListAction |
  FetchAddUserAction |
  AddUserAction |
  FetchDeleteUserAction |
  DeleteUserAction

export type UserState = {
  users: User[]
}

const initState = {
  users: []
}

export default function userReducer(state: UserState = initState, action: Action): UserState {
  switch (action.type) {
    case ActionType.UPDATE_USER_LIST:
      return {
        ...state,
        users: action.users
      }
    case ActionType.ADD_USER:
      return {
        ...state,
        users: [
          ...state.users,
          action.user
        ]
      }
    case ActionType.DELETE_USER:
      return {
        ...state,
        users: state.users.filter((u) => u.id !== action.user.id)
      }
    case ActionType.FETCH_USER_LIST:
    case ActionType.FETCH_ADD_USER:
    case ActionType.FETCH_DELETE_USER:
    default:
      return state
  }
}
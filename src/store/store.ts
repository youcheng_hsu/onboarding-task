import { applyMiddleware, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'
import userReducer from './reducer'
import userSaga from './sagas'

const SagaMiddleware = createSagaMiddleware()
const Store = createStore(userReducer, applyMiddleware(SagaMiddleware))
SagaMiddleware.run(userSaga)
export default Store
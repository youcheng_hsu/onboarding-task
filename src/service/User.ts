import axios from 'axios'
import { User } from '../types/User'

const PATH = 'http://localhost:8000'

export function getUserList(): Promise<User[]> {
  return axios.get(`${PATH}/users`).then(res => res.data)
}

export function addNewUser(user: Omit<User, 'id'>): Promise<User> {
  return axios.post(`${PATH}/users`, user).then(res => res.data)
}

export function deleteUser(user: User): Promise<void> {
  return axios.delete(`${PATH}/users/${user.id}`).then(() => {})
}
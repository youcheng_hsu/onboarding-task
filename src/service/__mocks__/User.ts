import { User } from '../../types/User'

export function getUserList(): Promise<User[]> {
  return Promise.resolve([{
    "id": 1,
    "first_name": "Sherlock",
    "last_name": "Holmes",
    "gender": "male",
    "age": 39,
    "address": "221B Baker Street"
  }])
}

export function addNewUser(user: Omit<User, 'id'>): Promise<User> {
  return Promise.resolve({
    ...user,
    id: Date.now()
  })
}

export function deleteUser(user: User): Promise<void> {
  return Promise.resolve()
}